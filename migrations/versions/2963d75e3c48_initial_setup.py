""" Initial setup.

Revision ID: 2963d75e3c48
Revises: None
Create Date: 2015-05-02 14:52:33.689049

"""

# revision identifiers, used by Alembic.
revision = '2963d75e3c48'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('users',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('username', sa.String(length=80), nullable=False),
                    sa.Column('email', sa.String(length=80), nullable=False),
                    sa.Column('password', sa.String(length=128), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=False),
                    sa.Column('first_name', sa.String(length=30), nullable=True),
                    sa.Column('last_name', sa.String(length=30), nullable=True),
                    sa.Column('active', sa.Boolean(), nullable=True),
                    sa.Column('is_admin', sa.Boolean(), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('email'),
                    sa.UniqueConstraint('username'))

    op.create_table('roles',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=80), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name'))


def downgrade():
    op.drop_table('roles')
    op.drop_table('users')