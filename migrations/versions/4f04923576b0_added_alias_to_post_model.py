"""Added 'alias' to Post-model.

Revision ID: 4f04923576b0
Revises: 224d8f2b148
Create Date: 2015-05-03 00:04:08.020048

"""

# revision identifiers, used by Alembic.
revision = '4f04923576b0'
down_revision = '224d8f2b148'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('post', sa.Column('alias', sa.String(length=50)))


def downgrade():
    op.drop_column('post', 'alias')
