"""Added model for Post.

Revision ID: 224d8f2b148
Revises: 2963d75e3c48
Create Date: 2015-05-02 17:46:57.854108

"""

# revision identifiers, used by Alembic.
revision = '224d8f2b148'
down_revision = '2963d75e3c48'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('post',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.Column('text', sa.Text(), nullable=True),
                    sa.Column('picture', sa.String(length=100), nullable=True),
                    sa.PrimaryKeyConstraint('id'))


def downgrade():
    op.drop_table('post')
