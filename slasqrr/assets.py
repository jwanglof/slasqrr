# -*- coding: utf-8 -*-
from flask.ext.assets import Bundle, Environment

css = Bundle(
    "libs/bootstrap/dist/css/bootstrap.min.css",
    "css/animate.css",
    "css/style.css",
    filters="cssmin",
    output="public/css/common.css"
)

rpi_css = Bundle(
    "libs/bootstrap/dist/css/bootstrap.min.css",
    "css/rpi_style.css",
    filters="cssmin",
    output="public/css/rpi_common.css"
)

js = Bundle(
    "libs/jQuery/dist/jquery.min.js",
    "libs/bootstrap/dist/js/bootstrap.min.js",
    "js/plugins.js",
    "js/script.js",
    filters='jsmin',
    output="public/js/common.js"
)

rpi_js = Bundle(
    "libs/jQuery/dist/jquery.min.js",
    "libs/bootstrap/dist/js/bootstrap.min.js",
    "js/rpi_script.js",
    filters='jsmin',
    output="public/js/rpi_common.js"
)

assets = Environment()

assets.register("js_all", js)
assets.register("css_all", css)
assets.register("js_rpi", rpi_js)
assets.register("css_rpi", rpi_css)
