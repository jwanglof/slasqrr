# -*- coding: utf-8 -*-
'''Public section, including homepage and signup.'''
from datetime import datetime as dt
from PIL import Image

from flask import (Blueprint, request, render_template, flash, url_for,
                   redirect, current_app)
from flask.ext.login import login_required, logout_user
from jinja2 import utils

from slasqrr.extensions import login_manager, photos
from slasqrr.user.models import User, Post
from slasqrr.public.forms import LoginForm, PostForm
from slasqrr.user.forms import RegisterForm
from slasqrr.utils import flash_errors


blueprint = Blueprint('public', __name__, static_folder="../static")


@login_manager.user_loader
def load_user(id):
    return User.get_by_id(int(id))


@blueprint.route("/", methods=["GET", "POST"])
def home():
    form = PostForm(request.form)
    is_prod = current_app.config['ENV'] == 'prod'

    if request.method == 'POST':
        if form.validate_on_submit():
            if 'picture' in request.files \
                    and request.files.get('picture').filename is not None \
                    and request.files.get('picture').filename != '':
                filename = str(utils.escape(photos.save(request.files['picture'])))
                extension = filename.rsplit('.', 1)[1]

                size = 500, 500
                long_filename = current_app.config['UPLOADS_DEFAULT_DEST'] + '/photos/' + filename
                im = Image.open(long_filename)
                im.thumbnail(size)
                filename_thumbnail = filename + '_resize.' + extension
                im.save(current_app.config['UPLOADS_DEFAULT_DEST'] + '/photos/' + filename_thumbnail)

            else:
                filename = None
                filename_thumbnail = None
                # If a picture does not exist, than there must exist a message.
                if 'message' not in request.form or request.form['message'] == '':
                    flash(u'Du måste antingen ladda upp en bild, ett meddelande eller båda. '
                          u'Nu skickade du ju inget, bängarsle!', 'warning')
                    return render_template("public/home.html", form=form, prod=is_prod)

            Post.create(alias=str(utils.escape(request.form['alias'])),
                        text=str(utils.escape(request.form.get('message', None))),
                        picture=filename_thumbnail if filename_thumbnail else None)

            flash(u'Nu är posten uppladdad. Håll koll på TVn!', 'success')

            redirect_url = request.args.get("next") or url_for("rpi.rpi_client")
            return redirect(redirect_url)
        else:
            flash_errors(form)

    if dt.now() < dt(2015, 5, 20, 18, 0, 0):
        return render_template("public/countdown.html", prod=is_prod)

    return render_template("public/home.html", form=form, prod=is_prod)


@blueprint.route("/staben_skd", methods=["GET", "POST"])
def home_skd():
    form = PostForm(request.form)
    is_prod = current_app.config['ENV'] == 'prod'

    if request.method == 'POST':
        if form.validate_on_submit():
            if 'picture' in request.files \
                    and request.files.get('picture').filename is not None \
                    and request.files.get('picture').filename != '':
                filename = photos.save(request.files['picture'])
            else:
                filename = None
                # If a picture does not exist, than there must exist a message.
                if 'message' not in request.form or request.form['message'] == '':
                    flash(u'Du måste antingen ladda upp en bild, ett meddelande eller båda. '
                          u'Nu skickade du ju inget, bängarsle!', 'warning')
                    return render_template("public/home.html", form=form, prod=is_prod)

            Post.create(alias=str(utils.escape(request.form['alias'])),
                        text=str(utils.escape(request.form.get('message', None))),
                        picture=str(utils.escape(filename)) if filename else None)

            flash(u'Nu är posten uppladdad. Håll koll på TVn!', 'success')

            redirect_url = request.args.get("next") or url_for("rpi.rpi_client")
            return redirect(redirect_url)
        else:
            flash_errors(form)

    return render_template("public/home.html", form=form, prod=is_prod)


@blueprint.route('/logout/')
@login_required
def logout():
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('public.home'))


@blueprint.route("/register/", methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form, csrf_enabled=False)
    if form.validate_on_submit():
        new_user = User.create(username=form.username.data,
                               email=form.email.data,
                               password=form.password.data,
                               active=True)
        flash("Thank you for registering. You can now log in.", 'success')
        return redirect(url_for('public.home'))
    else:
        flash_errors(form)
    return render_template('public/register.html', form=form)


@blueprint.route("/about/")
def about():
    form = LoginForm(request.form)
    return render_template("public/about.html", form=form)
