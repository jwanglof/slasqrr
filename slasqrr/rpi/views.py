#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, jsonify, current_app
from slasqrr.extensions import photos

from slasqrr.user.models import Post


blueprint = Blueprint('rpi', __name__, static_folder='../static', url_prefix='/pi')


@blueprint.route('/', methods=['GET'])
def rpi_client():
    is_prod = current_app.config['ENV'] == 'prod'
    return render_template('rpi/view.html', prod=is_prod)


@blueprint.route('/<post_id>', methods=['GET'])
def posts(post_id):
    if post_id > 0:
        data = Post.query.filter(Post.id > post_id).limit(15).all()
    else:
        data = Post.query.limit(15).all()

    posts = []
    for post in data:
        url = photos.url(post.picture) if post.picture is not None else ''
        posts.append({'id': post.id, 'message': post.text, 'picture': url, 'alias': post.alias})

    return jsonify(posts=posts)

