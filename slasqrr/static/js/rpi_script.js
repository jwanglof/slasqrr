(function($, window) {

    var pollSeconds = 60;
    var prevLength = 0, latestPostId = 0;
    var posts = [];

    console.log("STABEN ser dig, alltid.");

    function pollServer() {

        //console.log("Polling server.");

        function successCallback(data, textStatus, jqXHR) {

            //console.log("Success.");

            var newPosts = data.posts;
            var maxId = latestPostId;

            for(var i = 0; i < newPosts.length; i++) {
                // Store highest ID (i.e. the latest post).
                maxId = newPosts[i].id > maxId ? newPosts[i].id : maxId;
                posts.push(newPosts[i]);
            }

            latestPostId = maxId;

            updateDOM();
        }

        function newTimeout() {
            setTimeout(pollServer, pollSeconds * 1000);
        }

        $.get('/pi/' + latestPostId, successCallback).always(newTimeout);
    }

    function updateDOM() {

        function createPost(post) {
            var img = '';
            var text = '';

            if (post.picture) {
                img = '<img class="img-responsive img-thumbnail" src="' + post.picture + '" alt=""></img>';
            }

            if (post.message) {

                // If post contains both text and picture, make the text a .carousel-caption.
                if (post.picture) {
                    text = '<div class="carousel-caption"><h1>' + post.message + '</h1></div>';
                } else {
                    text = '<h1 class="text-center">' + post.message + ' - ' + post.alias + '</h1>';
                }
            }

            return '<div class="item">' + img + text + '</div>';
        }

        for(var i = prevLength; i < posts.length; i++) {
            $('#items').append(createPost(posts[i]));
        }

        prevLength = posts.length;

    }

    pollServer();


}).call(this, jQuery, window);